from Bio import SeqIO
import numpy as np


class ForwardAlgorithm:
    def __init__(self, seqs, genProb, delta=0.05, q=0.25, eps=0.1, tau=0.2, aM=0.5, aXY=0.8):
        if len(seqs[0]) >= len(seqs[1]):
            self.seq1 = seqs[0]
            self.seq2 = seqs[1]
        else:
            self.seq1 = seqs[1]
            self.seq2 = seqs[0]
        self.delta = delta
        self.q = q
        self.epsilon = eps
        self.tau = tau
        self.aM = aM
        self.aXY = aXY
        self.genProb = genProb

        self.matrix = np.empty((len(self.seq1) + 1, len(self.seq2) + 1, 3))
        self.matrix[0][0] = [0, -np.inf, -np.inf]
        self.matrix[0][1] = [-np.inf, round(self.matrix[0][0][0] + np.log(self.delta) + np.log(self.q), 3), -np.inf]
        self.matrix[1][0] = [-np.inf, -np.inf, self.matrix[0][1][1]]
        for letterId in range(2, len(self.seq2) + 1):
            self.matrix[0][letterId] = [-np.inf,
                                        round(self.matrix[0][letterId - 1][1] + np.log(self.epsilon) + np.log(self.q),
                                              3), -np.inf]
            self.matrix[letterId][0] = [-np.inf, -np.inf, self.matrix[0][letterId][1]]
        for letterId in range(len(self.seq2) + 1, len(self.seq1) + 1):
            self.matrix[letterId][0] = [-np.inf, -np.inf,
                                        round(self.matrix[letterId - 1][0][2] + np.log(self.epsilon) + np.log(self.q),
                                              3)]

    def MatrixBuilding(self):
        matrix = self.matrix

        for i in range(1, len(self.seq1) + 1):
            for j in range(1, len(self.seq2) + 1):
                temp = self.aM * np.exp(matrix[i - 1][j - 1][0])
                temp += self.aXY * (np.exp(matrix[i - 1][j - 1][1]) + np.exp(matrix[i - 1][j - 1][2]))
                temp = np.log(temp)
                Vm = temp + (self.genProb.get(self.seq1[i - 1] + self.seq2[j - 1]) if self.genProb.get(self.seq1[i - 1] + self.seq2[j - 1]) is not None else self.genProb.get(self.seq2[j - 1] + self.seq1[i - 1]))

                temp = self.delta * np.exp(matrix[i - 1][j][0])
                temp += self.epsilon * np.exp(matrix[i - 1][j][1])
                temp = np.log(temp)
                Vx = temp + np.log(self.q)

                temp = self.delta * np.exp(matrix[i][j - 1][0])
                temp += self.epsilon * np.exp(matrix[i][j - 1][2])
                temp = np.log(temp)
                Vy = temp + np.log(self.q)

                matrix[i][j] = [round(Vm, 3), round(Vx, 3), round(Vy, 3)]

    def PrintMatrix(self):
        matrix = self.matrix
        print("Матрица динамического программирования:\n")
        for i, _ in enumerate(" " + self.seq1):
            for j, _ in enumerate(" " + self.seq2):
                print(matrix[i][j], end='  |  ')
            print('\n')

        print("Вероятность порождения:",
              self.aM * np.exp(matrix[-1][-1][0]) + self.aXY * (np.exp(matrix[-1][-1][1]) + np.exp(matrix[-1][-1][2])))


def Validate(seq):
    alphabet = 'ACGT'
    for i in seq:
        if i not in alphabet:
            return False
        else:
            pass
    return True


def Read(seqs, name, genProb):
    for record in SeqIO.parse(name, "fasta"):
        seqs.append(record.seq)
    if len(seqs) > 2:
        return False
    if not Validate(seqs[0]):
        return False
    if not Validate(seqs[1]):
        return False
    with open('transProbs.txt') as f:
        symbols = f.readline()[:-1]
        while symbols != "":
            symbols = symbols.split(" ")
            genProb[symbols[0]] = float(symbols[1])
            symbols = f.readline()[:-1]
    return True


if __name__ == '__main__':
    seqs = []
    genProb = {}
    check = Read(seqs, "seqs.txt", genProb)
    if not check:
        print("Некорректно введённые данные")
        exit()
    align = ForwardAlgorithm(seqs, genProb)
    align.MatrixBuilding()
    align.PrintMatrix()
