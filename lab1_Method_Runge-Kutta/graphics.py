import matplotlib.pyplot as plt
from main import RealSolution
from numpy import *


def Graphic(string1, string2, values, epsilons):
    plt.xlabel(string1)
    plt.ylabel(string2)
    plt.loglog(epsilons, values)
    plt.legend(["полученные результаты"])
    plt.show()


def GraphicForDifferentEps(tk, xyz, epsilon, changeStep):
    label = f" при точночти {str(epsilon)}"
    plt.xlabel("t")
    plt.ylabel("Step" + label)
    tk2 = []
    x = []
    y = []
    z = []
    realX = []
    realY = []
    realZ = []
    for id, node in enumerate(tk[:-1]):
        tk2.append((node + tk[id + 1]) / 2)
        realXYZ = RealSolution(node)
        realX.append(realXYZ[0])
        realY.append(realXYZ[1])
        realZ.append(realXYZ[2])
        x.append(xyz[id][0])
        y.append(xyz[id][1])
        z.append(xyz[id][2])
    x.append(xyz[-1][0])
    y.append(xyz[-1][1])
    z.append(xyz[-1][2])
    realXYZ = RealSolution(tk[-1])
    realX.append(realXYZ[0])
    realY.append(realXYZ[1])
    realZ.append(realXYZ[2])
    plt.semilogy(tk2, changeStep)
    plt.legend(["полученные результаты"])
    plt.show()
    plt.xlabel("t")
    plt.ylabel("X, Y, Z" + label)
    plt.semilogy(tk[1:], fabs(array(x) - array(realX))[1:], tk[1:], fabs(array(y) - array(realY))[1:], tk[1:], fabs(array(z) - array(realZ))[1:])
    plt.legend(["модуль разности Х", "Y", "Z"])
    plt.show()


def ReadSteps(f):
    line = f.readline()[:-1]
    epsilons = []
    minH = []
    stepsNum = []
    while line[0] != "-":
        data = line.split("\t")
        minH.append(float(data[0]))
        stepsNum.append(int(data[1]))
        epsilons.append(float(data[2]))
        line = f.readline()[:-1]
    return minH, stepsNum, epsilons


def ReadForDiffereantEps(f):
    epsilon = float(f.readline()[:-1])
    line = f.readline()[:-1]
    cnt = 0
    tk = []
    xyz = []
    stepChange = []
    while line[0] != "-":
        data = line.split("\t")
        tk.append(float(data[0]))
        xyz.append(list(map(float, data[1][1:-1].split(" "))))
        stepChange.append(float(data[2]))
        cnt += 1
        line = f.readline()[:-1]
    stepChange = stepChange[:-1]
    return epsilon, tk, xyz, stepChange


if __name__ == '__main__':
    f = open("Results.txt", "r")
    minH, stepsNum, epsilons = ReadSteps(f)
    Graphic("epsilon", "Min step", minH, epsilons)
    Graphic("epsilon", "Number of steps", stepsNum, epsilons)
    epsilon, tk, xyz, stepChange = ReadForDiffereantEps(f)
    GraphicForDifferentEps(tk, xyz, epsilon, stepChange)
    epsilon, tk, xyz, stepChange = ReadForDiffereantEps(f)
    GraphicForDifferentEps(tk, xyz, epsilon, stepChange)
    epsilon, tk, xyz, stepChange = ReadForDiffereantEps(f)
    GraphicForDifferentEps(tk, xyz, epsilon, stepChange)
    f.close()

