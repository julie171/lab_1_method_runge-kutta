from numpy import *
# Метод Рунге-Кутты 2 порядка с условиями на левом конце


EQSCOUNT = 3
DETERMRUNGE = 2 ** 2 - 1
dxdydz = array([[-1, 1, 1],
                [1, -1, 1],
                [1, 1, -1]])


def RealSolution(t):
    x = exp(t) + 2 * exp(-2 * t)
    y = exp(t) + 1 * exp(-2 * t)
    z = exp(t) - 3 * exp(-2 * t)
    return array([x, y, z])


def SystemDiffEqs(t, arr):
    return dxdydz @ arr


def NewXYZ(t, funcValues, h):
    k1 = SystemDiffEqs(t, funcValues)
    k2 = SystemDiffEqs(t + h, funcValues + h * k1)
    newFuncValues = funcValues + h / 2 * (k1 + k2)
    return newFuncValues


def RKIteration(h, tk, xyzk):
    arrXYZ = array([xyzk])
    for _ in range(2):
        arrXYZ = vstack([arrXYZ, NewXYZ(tk, arrXYZ[-1], h)])
        tk += h
    return arrXYZ[1:]


def MethodRungeKutta(initCond, a, b, eps):
    nodesCount = 11
    h0 = (b - a) / (nodesCount - 1)
    xyz = array([initCond])
    stepChange = []
    newH = h0
    minH = h0
    points = array([a])
    while points[-1] < b:
        funcValuesH = NewXYZ(points[-1], xyz[-1], newH)
        newH /= 2.0
        funcValuesH2 = RKIteration(newH, points[-1], xyz[-1])
        while (norma := linalg.norm(funcValuesH - funcValuesH2[-1], inf) / DETERMRUNGE) > eps:
            funcValuesH = funcValuesH2[0]
            newH /= 2.0
            funcValuesH2 = RKIteration(newH, points[-1], xyz[-1])
        stepChange.append(2.0 * newH)
        if minH > newH:
            minH = 2.0 * newH
        points = append(points, points[-1] + 2.0 * newH)
        xyz = vstack([xyz, funcValuesH2[-1]])
        if norma < eps / 64:
            newH *= 2
    return points, xyz, minH, (b - a) / minH, stepChange


def PrintInFile(f, epsilon, a, b, initCond):
    f.write("-----------------------------------------------\n")
    f.write(f"{epsilon}\n")
    points, xyz, minH, stepsNum, stepChange = MethodRungeKutta(initCond, a, b, epsilon)
    for id, node in enumerate(points[:-1]):
        f.write(f"{node:.15}\t[{xyz[id, 0]:.15f} {xyz[id, 1]:.15f} {xyz[id, 2]:.15f}]\t{stepChange[id]}\n")
    f.write(f"{points[-1]:.15}\t[{xyz[-1, 0]:.15f} {xyz[-1, 1]:.15f} {xyz[-1, 2]:.15f}]\t0\n")


if __name__ == '__main__':
    a = 0
    b = 2
    initCond = RealSolution(a)
    epsilons = [10 ** i for i in range(-1, -11, -1)]
    f = open("Results.txt", "w")
    for eps in epsilons:
        points, xyz, minH, stepsNum, stepChange = MethodRungeKutta(initCond, a, b, eps)
        f.write(f"{minH:.15f}\t{int(stepsNum)}\t{eps}\n")
    PrintInFile(f, epsilons[1], a, b, initCond)
    PrintInFile(f, epsilons[4], a, b, initCond)
    PrintInFile(f, epsilons[7], a, b, initCond)
    f.write("-----------------------------------------------\n")
    f.close()
