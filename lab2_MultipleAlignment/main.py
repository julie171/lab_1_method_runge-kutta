from Bio import SeqIO


class BackwardAlgorithm:
    def __init__(self, seq):

        self.stateNum = 4

        self.matchProb = {'A': [0, 0.25, 0.55, 0.08],
                          'C': [0, 0.08, 0.09, 0.33],
                          'G': [0, 0.58, 0.18, 0.08],
                          'T': [0, 0.08, 0.18, 0.08]}
        self.insertProb = {'A': [0.25, 0.17, 0.25, 0.25],
                           'C': [0.25, 0.5, 0.25, 0.25],
                           'G': [0.25, 0.17, 0.25, 0.25],
                           'T': [0.25, 0.17, 0.25, 0.25]}
        self.transProb = {'M-M': [0.82, 0.55, 0.8, 0.9],
                          'M-D': [0.09, 0.18, 0.1, 0.09],
                          'M-I': [0.09, 0.27, 0.1, 0.1],
                          'I-M': [0.33, 0.6, 0.33, 0.5],
                          'I-D': [0.33, 0.2, 0.33, 0.33],
                          'I-I': [0.33, 0.2, 0.33, 0.5],
                          'D-M': [0, 0.33, 0.5, 0.5],
                          'D-D': [0, 0.33, 0.25, 0.33],
                          'D-I': [0, 0.33, 0.25, 0.5]}
        # вероятности выхода
        self.eps = [[0.0, 0.5, 0.3, 0.2], [0.1, 0.4, 0.2, 0.3], [0.0, 0.1, 0.3, 0.6]]

        self.prob0 = [[0.2, 0.1, 0.1, 0.2], [0.4, 0.4, 0.1, 0.3], [0.0, 0.1, 0.2, 0.7]]

        self.seq = seq
        self.seqLen = len(seq)
        self.matrix = []
        # инициализация
        self.matrix.append([[self.eps[0][-1]], [self.eps[0][-2]], [self.eps[0][-3]], [0]])
        self.matrix.append([[self.eps[1][-1]], [self.eps[1][-2]], [self.eps[1][-3]], [self.eps[1][-4]]])
        self.matrix.append([[self.eps[2][-1]], [self.eps[2][-2]], [self.eps[2][-3]], [0]])

    def fillingMatrix(self):
        for i in range(self.stateNum - 1, -1, -1):  # для всех состояний
            for j in range(self.seqLen - 1, 0, -1):  # для всех букв
                if i != 0:
                    bM = self.matchProb[self.seq[j]][i] * self.matrix[0][self.stateNum - i - 1][self.seqLen - j - 1] * \
                         self.transProb['M-M'][i] + \
                         self.insertProb[self.seq[j]][i] * self.matrix[1][self.stateNum - i - 1][self.seqLen - j - 1] * \
                         self.transProb['M-I'][i] + \
                         self.matrix[2][self.stateNum - i - 1][self.seqLen - j - 1] * self.transProb['M-D'][i]
                    self.matrix[0][self.stateNum - i - 1].append(bM)
                    bD = self.matchProb[self.seq[j]][i] * self.matrix[0][self.stateNum - i - 1][self.seqLen - j - 1] * \
                         self.transProb['D-M'][i] + \
                         self.insertProb[self.seq[j]][i] * self.matrix[1][self.stateNum - i - 1][self.seqLen - j - 1] * \
                         self.transProb['D-I'][i] + \
                         self.matrix[2][self.stateNum - i - 1][self.seqLen - j - 1] * self.transProb['D-D'][i]
                    self.matrix[2][self.stateNum - i - 1].append(bD)
                else:
                    self.matrix[0][self.stateNum - i - 1].append(0)
                    self.matrix[2][self.stateNum - i - 1].append(0)
                bI = self.matchProb[self.seq[j]][i] * self.matrix[0][self.stateNum - i - 1][self.seqLen - j - 1] * \
                     self.transProb['I-M'][i] + \
                     self.insertProb[self.seq[j]][i] * self.matrix[1][self.stateNum - i - 1][self.seqLen - j - 1] * \
                     self.transProb['I-I'][i] + \
                     self.matrix[2][self.stateNum - i - 1][self.seqLen - j - 1] * self.transProb['I-D'][i]
                self.matrix[1][self.stateNum - i - 1].append(bI)

    def printing(self):

        print('Матрица динамического программирования:\n')

        print(" ", end="")
        for j in range(self.seqLen - 1, -1, -1):
            print("               ", self.seq[j], "               ", end="")
        for i in range(self.stateNum - 1, -1, -1):
            print("\n", i, ': ', sep="", end="")
            for j in range(self.seqLen - 1, -1, -1):
                print("[ %.6f" % self.matrix[0][self.stateNum - i - 1][self.seqLen - j - 1],
                      "%.6f" % self.matrix[1][self.stateNum - i - 1][self.seqLen - j - 1],
                      "%.6f" % self.matrix[2][self.stateNum - i - 1][self.seqLen - j - 1], "]", end=' | ')

        p = self.matrix[1][-1][-1] * self.insertProb[self.seq[0]][0] * self.prob0[1][0]
        p += self.matrix[0][-1][-1] * self.matchProb[self.seq[0]][0] * self.prob0[0][0]
        p += self.matrix[2][-1][-1] * self.prob0[2][0]
        print('\nИтоговая вероятность: ', p)


def Validate(seq):
    alphabet = 'ACGT'
    for i in seq:
        if i not in alphabet:
            return False
        else:
            pass
    return True


def Read(seqs, name):
    for record in SeqIO.parse(name, "fasta"):
        seqs.append(record.seq)
    if len(seqs) != 1:
        return False
    if not Validate(seqs[0]):
        return False
    return True


if __name__ == '__main__':
    seqs = []
    check = Read(seqs, "seq.txt")
    if not check:
        print("Некорректно введённые данные")
        exit()
    print('Последовательность: ', seqs[0])

    align = BackwardAlgorithm(seqs[0])
    align.fillingMatrix()
    align.printing()
