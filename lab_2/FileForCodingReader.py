from contextlib import contextmanager
from FileReaderInterface import FileReaderInterface
from ConfigException import ConfigException


class FileForCodingReader(FileReaderInterface):
    @contextmanager
    def read_file(self, file_name: str, buffer_size: int):
        try:
            f = open(file_name, "r", buffering=buffer_size)
            yield f.readline()
        except Exception as e:
            raise ConfigException("Invalid file path")
        finally:
            f.close()

