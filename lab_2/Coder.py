from CoderInterface import CoderInterface


class Coder(CoderInterface):
    def run(self, coder_info: str, string_to_process: str) -> str:
        if coder_info == "code":
            return self._code(string_to_process)
        return self._decode(string_to_process)

    def _code(self, string_to_code: str) -> str:
        finalString = []
        for symbol in string_to_code:
            if not symbol.isalpha():
                finalString.append(symbol)
                continue
            if symbol == "z" or symbol == "Z":
                finalString.append(chr(ord(symbol) - (ord("Z") - ord("A"))))
            else:
                finalString.append(chr(ord(symbol) + 1))
        return "".join(finalString)

    def _decode(self, string_to_decode: str) -> str:
        finalString = []
        for symbol in string_to_decode:
            if not symbol.isalpha():
                finalString.append(symbol)
                continue
            if symbol == "a" or symbol == "A":
                finalString.append(chr(ord(symbol) + (ord("Z") - ord("A"))))
            else:
                finalString.append(chr(ord(symbol) - 1))
        return "".join(finalString)