from ConfigException import ConfigException
from ConfigReaderInterface import ConfigReaderInterface


class ConfigForCodingReader(ConfigReaderInterface):

    def read_config(self, config_file_name: str) -> dict:
        with open(config_file_name, "r") as f:
            while string := f.readline():
                elements = string.split(self._param_delimiter)
                if self._config_dict.get(elements[0].strip(), False) != False:
                    self._config_dict[elements[0].strip()] = elements[1].strip(" |\n")
                else:
                    raise ConfigException("Invalid name of argument")

        if not self._config_dict[self.buffer_size_param_name].isdigit():
            raise ConfigException("Invalid value of the first argument")

        self._config_dict[self.buffer_size_param_name] = int(self._config_dict[self.buffer_size_param_name])

        if self._config_dict[self.coder_run_option_param_name] != "code" and self._config_dict[self.coder_run_option_param_name] != "decode":
            raise ConfigException("Invalid value of the last argument")

        return self._config_dict
