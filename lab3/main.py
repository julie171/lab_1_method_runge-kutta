from Bio import SeqIO
import numpy as np


class BaumWelch:
    def __init__(self):
        self.states = ['H', 'L']
        # вероятности
        self.a = {'00': 0.2, '01': 0.8, '10': 0.7, '11': 0.3}
        # 0 - H, 1 - L
        self.e = {'A': [0.25, 0.25], 'C': [0.25, 0.25], 'G': [0.25, 0.25], 'T': [0.25, 0.25]}

    def forwardAlgorithm(self, seq):
        L = len(seq)
        matrix = [[self.e[seq[0]][0], self.e[seq[0]][1]]]

        for i in range(1, L):
            matrix.append([0, 0])
            for l, state in enumerate(self.states):
                summ = matrix[i - 1][0] * self.a["0" + str(l)] + matrix[i - 1][1] * self.a["1" + str(l)]
                matrix[-1][l] = self.e[seq[i]][l] * summ

        p = 0
        for i, _ in enumerate(self.states):
            p += matrix[L - 1][i] * self.a[str(i) + '0']
        return p, matrix

    def backwardAlgorithm(self, seq):
        L = len(seq)
        matrix = np.zeros((len(self.states), L))

        matrix[:][-1] = 1

        for i in range(L - 2, -1, -1):
            for k, state in enumerate(self.states):
                summ = self.a[str(k) + "0"] * self.e[seq[i + 1]][0] * matrix[0][i + 1] + \
                       self.a[str(k) + "1"] * self.e[seq[i + 1]][1] * matrix[1][i + 1]
                matrix[k][i] = summ

        p = 0
        for i, _ in enumerate(self.states):
            p += matrix[0][i] * self.a['0'+str(i)] * self.e[seq[0]][i]

        return p, matrix

    def mainAlgorithm(self, seqs, iters=100, eps=1e-6):
        prevProb = -np.inf
        for iteration in range(iters):
            for seq in seqs:
                pF, f = self.forwardAlgorithm(seq)
                pB, b = self.backwardAlgorithm(seq)

                L = len(seq)
                Akl = np.zeros((len(self.states), len(self.states), L - 1))
                Ekb = np.zeros((len(self.states), L))

                for k in range(L):
                    denom = f[k][0] * b[0][k] + f[k][1] * b[1][k]
                    if pB != 0:
                        for i, _ in enumerate(self.states):
                            Ekb[i, k] = (f[k][i] * b[i][k]) / denom

                for k, _ in enumerate(self.states):
                    denom = f[k][0] * b[0][k] + f[k][1] * b[1][k]
                    for l, _ in enumerate(self.states):
                        for m in range(L - 1):
                            if pF != 0:
                                Akl[k, l, m] = (f[m][k] * self.a[str(k) + str(l)] * self.e[seq[m + 1]][l] * b[l][m + 1]) / denom

                for i, _ in enumerate(self.states):
                    for j, _ in enumerate(self.states):
                        summAkl = sum(Akl[i, j, t] for t in range(L - 1))
                        summEkb = sum(Ekb[i, t] for t in range(L - 1))
                        if summEkb != 0:
                            self.a[str(i) + str(j)] = summAkl / summEkb

                for j, _ in enumerate(self.states):
                    for letter in "ACGT":
                        numerator = sum(Ekb[i, t] for t in range(L) if seq[t] == letter)
                        denominator = sum(Ekb[i, t] for t in range(L))
                        if denominator != 0:
                            self.e[letter][j] = numerator / denominator

                currProb = self.findLogProb(seqs)

                if abs(currProb - prevProb) < eps:
                    return

                prevProb = currProb

    def findLogProb(self, seqs):
        logProb = 0.0
        for seq in seqs:
            p, f = self.forwardAlgorithm(seq)
            logProb += np.log(np.sum(f[-1][:]))

        return logProb

    def resultPrinting(self):
        print("Transmission probabilities:")
        print("HH -", self.a["00"])
        print("HL -", self.a["01"])
        print("LH -", self.a["10"])
        print("LL -", self.a["11"], "\n")
        print("Generation probabilities:")
        print("A -", self.e["A"])
        print("C -", self.e["C"])
        print("G -", self.e["G"])
        print("T -", self.e["T"])


def Validate(seq):
    alphabet = 'ACGT'
    for i in seq:
        if i not in alphabet:
            return False
        else:
            pass
    return True


def Read(seqs, name):
    for record in SeqIO.parse(name, "fasta"):
        seqs.append(record.seq)
    for seq in seqs:
        if not Validate(seq):
            return False
    return True


if __name__ == '__main__':
    seqs = []
    check = Read(seqs, "seqs.txt")
    if not check:
        print("Некорректно введённые данные")
        exit()
    profileHmm = BaumWelch()
    profileHmm.mainAlgorithm(seqs)
    profileHmm.resultPrinting()




